# -*- coding: utf-8 -*-

import pandas as pd
import numpy as np
from numpy.random import normal
import matplotlib.pyplot as plt

s0 = 448.12
t = 180
drift = .103
dt = 1/252
sigma = 0.1037

def geometric_brownian_motion():
    w_t = np.sqrt(dt) * sigma
    bm = np.array(normal(loc=0, scale=w_t, size=t))
    a_s = (drift - 0.5 * (sigma**2.0)) * dt
    s_t = np.exp(bm + a_s)
   
    price = [s0]
    for i in range(1, len(s_t)):
        price.append(price[i - 1] * s_t[i - 1])
    path = np.array(price)

    return path

def frame():
    df = pd.DataFrame(geometric_brownian_motion())
    df.rename(columns={0: 'close'}, inplace=True)
    print(df)
    return df

def save(df):
    df.to_csv(r'', index = False, header=True)

def plot(df, title, fname):
    fig, ax = plt.subplots()
    fig.suptitle(title, fontsize=16)
    ax.set_xlabel('Time')
    ax.set_ylabel('Price (USD)')
    x_axis = np.arange(0, len(df), 1)
    plt.plot(x_axis, df)
    plt.savefig(fname, bbox_inches='tight')

df = frame()
plot(df, 'GBM Test', 'gbmtest.pdf')
#save(df)
