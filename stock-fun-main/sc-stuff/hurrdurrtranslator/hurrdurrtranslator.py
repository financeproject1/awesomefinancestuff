from morsecode import translate_morse_code, detranslate_morse_code
import random

def translate(message):
    ''' Translates a normal message into hurr-durr message.

    Arguments:
    message: Message to send

    Returns: hurr-durr message translation

    Example:
    translated_message = translate("the meaning of life is 42")
    print(translated_message)
    '''
    for i in range(10):
        message = message.strip(' ')
        message = message.rstrip(' ')
        message = message.replace('  ', ' ')
    # Translate first to morse code:
    morse_code_message = translate_morse_code(message.upper())
    # Now the message looks like `..- --. / --.` etc, with / denoting spaces and combinations of `..-` mapping to letters. 
    # Now replace every `.` with u and every `-` with r. Furthermore, add randomly either 'h' or 'd' to the start of every morse code letter, and replace every '/' with '?'. Then, add `?` to the end:
    hurr_durr_code_message = morse_code_message.replace(".","u").replace("-","r").replace('  ','? ')
    hurr_durr_code_message = hurr_durr_code_message[:-1] + '?' # Add ? at the end
    # Now add the hurrs and durrs :D
    final_hurr = []
    for hurr in hurr_durr_code_message.split(' '):
        if hurr[0] == 'u':
            # Randomly add d or h
            winning_letter = ['h', 'd'][random.randint(0,1)] # drumroll......
            final_hurr.append(winning_letter + hurr)
        else:
             # Add 'h' (drr is not aesthetically pleasing and is therefore not part of the hurr-durr language)
            winning_letter = 'h'
            final_hurr.append(winning_letter + hurr)
    # Convert to string instead of a list
    translated_message = ' '.join(final_hurr)
    return translated_message

def detranslate(message):
    ''' Translates a magical hurr-durr message into a boring, normal message

    Arguments:
    message: Magical hurr-durr message

    Returns: Boring normal message every1 hates.

    Example:
    translated_message = translate("the meaning of life is 42")
    print(translated_message)
    '''
    # First trnaslate the message back to the morse code message:
    morse_message = message.replace('h','').replace('d','') # Remove all of the 'h' and 'd':
    morse_message = morse_message.replace('u','.').replace('r','-').replace('?','  ') # Replace 'u' wit '.' and 'r' with '-' to make this into morse code, and '?' with double space
    # Remove trailing whitespace and other weird typos people commonly make
    for i in range(10):
        morse_message = morse_message.strip(' ')
        morse_message = morse_message.rstrip(' ')
        morse_message = morse_message.replace('   ', '  ')
    #return morse_message
    final_boring_message = detranslate_morse_code(morse_message)
    return final_boring_message


# If the user wants to run this as a program:
if __name__ == '__main__':
    to_translate_or_not_to_translate = input("Translate or de-translate? [0=de-translate, 1=translate]")
    if int(to_translate_or_not_to_translate) == 0:
        function = detranslate
    elif int(to_translate_or_not_to_translate) == 1:
        function = translate
    else:
        raise ValueError("Invalid value. Crashing. Abort.")
    while(True):
        try:
            message = input("Message: ")
            print("> ",function(message))
            print("")
        except:
            ""

