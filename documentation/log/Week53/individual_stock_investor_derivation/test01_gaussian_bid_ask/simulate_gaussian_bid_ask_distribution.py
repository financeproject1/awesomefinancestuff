''' This is a file to simulate the bid and ask distribution as time evolves. 

'''

import numpy as np
import bisect
from numba import jit, njit
from matplotlib import pyplot as plt
from scipy.stats import norm

# Define the mean sell/buy prices
sell_mean_price = 70
purchase_mean_price = 60

def get_puchase_sell_sales(Ninvesters=int(1e4), seed=0):
    np.random.seed(seed)
    asks = [] # Want to sell
    bids = [] # want to buy
    sales_purchase = []
    sales_sell = []
    sales = [] 
    # Draw a random investor from a bid-ask spread
    purchase_price = np.random.normal(purchase_mean_price,5)
    sell_price = np.random.normal(sell_mean_price,5)
    # Add the investor in the bid-ask list
    bids.append(purchase_price)
    asks.append(sell_price)
    # Now draw 10^4 investors
    for i in range(Ninvesters):
        purchase_price = np.random.normal(purchase_mean_price,5)
        sell_price = np.random.normal(sell_mean_price,5)
        # If the purchase price is higher than the smallest ask price, then remove the highest ask price
        if asks[0]<=purchase_price:
            sales_purchase.append(asks.pop(0))
            sales.append(sales_purchase[-1])
        # Else if the sell price is lower than the highest bid price, then remove the lowest bid price
        elif sell_price <= bids[-1]:
            sales_sell.append(bids.pop())
            sales.append(sales_sell[-1])
        # Otherwise, add the investor in the bid-ask list in a sorted way
        else:
            bisect.insort(bids, purchase_price)
            bisect.insort(asks, sell_price)
    return bids, asks, sales_purchase, sales_sell, sales

bids, asks, sales_purchase, sales_sell, sales = get_puchase_sell_sales(int(1e4))
# Plot the bid-ask distribution as two histograms
plt.hist(bids, bins=100, alpha=0.5, label='bids', density=True, color = 'blue')
plt.hist(asks, bins=100, alpha=0.5, label='asks', density=True, color = 'red')
# Plot the actual underlying bid and ask distribution
x = np.linspace(0,100,1000)
plt.plot(x, norm.pdf(x, loc=60, scale=5), color='blue')
plt.plot(x, norm.pdf(x, loc=70, scale=5), color='red')
plt.legend(loc='upper right')
plt.xlabel('Price')
plt.ylabel('Distribution of investors')
plt.tight_layout()
plt.savefig('bid_ask_distribution.pdf', bbox_inches='tight')
plt.show()

# Plot the sales distribution
bids, asks, sales_purchase, sales_sell, sales = get_puchase_sell_sales(int(1e5))
plt.hist(sales_purchase, bins=100, alpha=0.5, label='sales (buy)', density=True)
plt.hist(sales_sell, bins=100, alpha=0.5, label='sales (sell)', density=True)
#plt.hist(sales, bins=50, alpha=0.5, label='sales', density=True)
# bids, asks, sales_purchase, sales_sell, sales = get_puchase_sell_sales(int(1e6))
# plt.hist(sales_purchase, bins=100, alpha=0.5, label='sales', density=True)
plt.legend(loc='upper right')
plt.xlabel("Price")
plt.ylabel("Distribution of sales")
plt.tight_layout()
plt.savefig('sales_distribution.pdf', bbox_inches='tight')
plt.show()

plt.hist(sales, bins=100, alpha=0.5, label='sales (buy)', density=True)
plt.legend(loc='upper right')
plt.xlabel("Price")
plt.ylabel("Distribution of sales")
plt.tight_layout()
plt.savefig('sales_all_distribution.pdf', bbox_inches='tight')
plt.show()


# Plot the sales evolution
bids, asks, sales_purchase, sales_sell, sales = get_puchase_sell_sales(int(1e3))
plt.plot(sales_purchase, label='sales (buy)')
plt.plot(sales_sell, label='sales (sell)')
plt.plot(sales, label='sales (all)')
plt.xlabel("Time")
plt.ylabel("Price")
plt.legend()
plt.tight_layout()
plt.savefig('sales_evolution.pdf', bbox_inches='tight')
plt.show()

